#include <algorithm>
#include <cmath>
#include <cstdio>

#include <cstring>
#include <iostream>
#include <map>

#include <queue>
#include <set>
#include <string>
#include <vector>
#include <functional>

using namespace std;

#define MAX_N 50000

int main(int argc, char** argv) {
    std::vector<int> v;
    for (int i = 0; i < MAX_N; i++) {
        v.push_back(i);
    }

    random_shuffle(v.begin(), v.end());

    cout << MAX_N << endl;
    for (int i = 0; i < MAX_N; i++) {
        cout << v[i] << endl;
    }


    return 0;
}
