package com.agsoares.t2;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MainActivity extends AppCompatActivity {


    //MergeSort
    public static int[] Iterative(int[] array) {
        for (int i = 1; i <= array.length; i *= 2) {
            for (int j = i; j < array.length; j += 2 * i) {
                Merge(array, j - i, j, Math.min(j + i, array.length));
            }
        }
        return array;
    }

    private static void Merge(int[] array, int start, int middle, int end) {
        int[] merge = new int[end-start];
        int l = 0, r = 0, i = 0;
        while (l < middle - start && r < end - middle) {
            merge[i++] = array[start + l] <= array[middle + r] ? array[start + l++] : array[middle + r++];
        }

        while (r < end - middle) merge[i++] = array[middle + r++];

        while (l < middle - start) merge[i++] = array[start + l++];

        System.arraycopy(merge, 0, array, start, merge.length);
    }

    //SelectionSort
    public static int[] SelectionSort(int[] arr){
        for (int i = 0; i < arr.length - 1; i++)
        {
            int index = i;
            for (int j = i + 1; j < arr.length; j++)
                if (arr[j] < arr[index])
                    index = j;

            int smallerNumber = arr[index];
            arr[index] = arr[i];
            arr[i] = smallerNumber;
        }
        return arr;
    }

    int[] array;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        array = readFile();

        Context context = getApplicationContext();
        CharSequence text = "File Read";
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);

    }

    @TargetApi(19)
    int[] readFile() {
        InputStream is = getResources().openRawResource(R.raw.shufflexl);
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF8")) ) {
            int n = Integer.parseInt(reader.readLine());
            int[] array = new int[n];
            for (int i = 0; i < n; i++) {
                array[i] = Integer.parseInt(reader.readLine());
            }
            return array;

        } catch (Exception e) {
            int [] array = { 0 };
            return array;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnSelectionClick(View v) {

        SelectionSort(array);

        Context context = getApplicationContext();
        CharSequence text = "SelectionSort Finish!" + " size: " + array.length;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }


    public void btnMergeClick(View v) {
        Iterative(array);

        Context context = getApplicationContext();
        CharSequence text = "MergeSort Finish!" + " size: " + array.length;
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

}
